NAME = document

all: build

build: #build_one build_bib build_index build_two
	pdflatex $(NAME).tex
	bibtex $(NAME).aux
	makeglossaries $(NAME)
	makeindex -s $(NAME).ist -t $(NAME).glg -o $(NAME).gls $(NAME).glo

	@echo
	@echo
	@echo "Glossaries finished. Rebuild latex."
	@echo

	pdflatex $(NAME).tex
	pdflatex $(NAME).tex

build_one:
	pdflatex $(NAME).tex

build_bib:
	bibtex $(NAME).aux

build_index:
	makeglossaries $(NAME)
	makeindex -s $(NAME).ist -t $(NAME).glg -o $(NAME).gls $(NAME).glo

clean:
	@rm -vf output/*
	@rm -vf *.log *.out *.aux *.pdf
	@rm -vf *.bbl *.blg
	@rm -vf *.gls* *.glo *.glg *.ist *.acn *.acr *.alg
	@rm -vf *.toc
	@rm -vf *.gz *.bak

.PHONY: all build build_one clean
